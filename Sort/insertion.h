#ifndef INSERTION_H
#define INSERTION_H

#include "sort.h"

class Insertion : public Sort
{
public:
    //Constructeur de base
    Insertion(std::vector<int> items);

    //Fonction de tri de base
    void compute_sort(std::vector<int> items);
};

#endif // INSERTION_H
