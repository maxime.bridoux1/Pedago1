#include "bubble.h"

Bubble::Bubble(std::vector<int> items) : Sort(items)
{
	compute_sort(items);
}

void Bubble::compute_sort(std::vector<int> items)
{
	bool has_changed = true;

	while (has_changed)
	{
		has_changed = false;
		for (int i = 0; i < items.size()-1; i++)
		{
            if (items[i] > items[i+1])
			{
				has_changed = true;
                swap(items, i, i+1);
                add_instruction_swap(i+1, i);
                add_instruction_color(i, Qt::red);
			}
            else
            {
                add_instruction_color(i, Qt::green);
            }
        }
        add_instruction_all_color(Qt::gray);
	}
    add_instruction_all_color(Qt::green);
}

