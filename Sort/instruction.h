#ifndef INSTRUCTION_H
#define INSTRUCTION_H

#include<QColor>

class Instruction
{
public:
    Instruction();
    Instruction(int a, int b, QColor c, int op);
    int get_a();
    int get_b();
    QColor get_color();
    int get_op();
private:
    int a;
    int b;
    QColor c;
    int op;
};

#endif // INSTRUCTION_H
