#ifndef SELECTION_H
#define SELECTION_H

#include "sort.h"

class Selection : public Sort
{
public:
    //Constructeur de base
    Selection(std::vector<int> items);

    //Fonction de tri de base
    void compute_sort(std::vector<int> items);
};

#endif // INSERTION_H
