#ifndef ANIMWINDOW_H
#define ANIMWINDOW_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGraphicsView>
#include <QPushButton>
#include <QLabel>
#include <QSlider>
#include <QLCDNumber>

#include <stdio.h>
#include "animinterface.h"
#include "sort_name_list.h"
#include "codeinterface.h"

class AnimWindow : public QWidget
{
    Q_OBJECT
public:
    explicit AnimWindow(QWidget *parent = nullptr, std::vector<int> _sort_list = std::vector<int>());
    void prepare();
    int get_selected_sort();
    void update_label_text();
    void update_next_buttons();
    void update_window();

private:
    //QPushButton *button_shuffle;
    QPushButton *button_next;
    QPushButton *button_previous;
    QPushButton *button_code;
    QLabel *label_current_sort;
    AnimInterface *anim_interface;
    CodeInterface *code_interface;
    std::vector<int> sort_list;
    int i_current_sort;
    bool ongoing;

    QLabel* number_of_items_label;
    QLCDNumber* number_of_items;
    QSlider* number_of_items_slider;

    QLabel* speed_label;
    QLCDNumber* speed;
    QSlider* speed_slider;


signals:

public slots:
    void sort();
    void step_by_step();
    void animation_ended();
    void order_shuffle();
    void next_sort();
    void previous_sort();
    void stop();
    void toggle_code();

};

#endif // ANIMWINDOW_H
