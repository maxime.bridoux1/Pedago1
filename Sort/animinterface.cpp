#include "animinterface.h"
#include <iostream>

AnimInterface::AnimInterface(QWidget *parent, int w, int h) :
    QWidget(parent)
{
    scene = new QGraphicsScene(this);
    init_items(35);
    view = new QGraphicsView(scene);
    view->setFixedSize(w, h);
    important = -1;
}

void AnimInterface::show_interface()
{
    view->viewport()->repaint();
    view->update();
    view->show();
}

void AnimInterface::init_items(unsigned int n)
{
    important = -1;
    items.clear();
    items_color.clear();
    for (unsigned int k = 0; k<n; k++)
    {
        items.push_back(k+1);
        items_color.push_back(QColor(Qt::gray));
    }
}

void AnimInterface::shuffle_items()
{
    for (unsigned int k = 0; k<items.size() - 1; k++)
    {
        int r = k+(rand()%(items.size() - k));
        int a = items[k];
        items[k] = items[r];
        items[r] = a;
    }
}

void AnimInterface::shuffle()
{
    shuffle_items();
    draw_items();
}

void AnimInterface::draw_items()
{
    scene->clear();
    int w = (view->width()-60)/items.size();
    int h = (view->height()-20)/items.size();
    QBrush brush;
    for (unsigned int k = 0; k < items.size(); k++)
    {
        brush = QBrush(items_color[k]);
        scene->addRect(k*w,(items.size()-items[k])*h,w,(items[k])*h,QPen(),brush);
    }
    show_interface();
}

void AnimInterface::prepare_sort(int i)
{
    in_memory = false;
    important = -1;
    Sort_name_list s;
    sort = s.create_sort(i, items);
    current_sort = i;
}

void AnimInterface::next_step()
{
    QCoreApplication::processEvents();

    if (sort.is_done())
    {
        emit sorted();
    }
    else
    {
        instruction = sort.get_next_instruction();

        if (instruction.get_op() == 0)
        {
            sort.swap(items, instruction.get_a(), instruction.get_b());
        }
        else
        {
            int i = instruction.get_a();
            QColor c = instruction.get_color();
            if (instruction.get_op() == 2)
            {
                for (int k = 0; k < items_color.size(); k++)
                {
                    items_color[k] = c;
                }
            }
            else
            {
                if (instruction.get_op() == 3)
                {
                    for (int k = i; k <= instruction.get_b(); k++)
                    {
                        items_color[k] = c;
                    }
                }
                else
                {
                    items_color[i] = c;
                }
            }

        }
    }
}

void AnimInterface::sort_items()
{
    while (!sort.is_done())
    {
        next_step();
        draw_items();
        usleep(1); // /!\ microsecondes
    }
    next_step();
    draw_items();
    next_step();
    draw_items();
}

void AnimInterface::init(int i)
{
    init_items(i);
}
