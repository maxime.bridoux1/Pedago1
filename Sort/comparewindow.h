#ifndef COMPAREWINDOW_H
#define COMPAREWINDOW_H

#include <QWidget>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QGraphicsView>
#include <QPushButton>
#include <QLabel>
#include <QSlider>
#include <QLCDNumber>

#include <stdio.h>
#include "animinterface.h"
#include "sort_name_list.h"

class CompareWindow : public QWidget
{
    Q_OBJECT
public:
    explicit CompareWindow(QWidget *parent = nullptr, std::vector<int> _sort_list = std::vector<int>());
    void prepare();
    int get_selected_sort();
    void update_label_text();
    void update_next_buttons();
    void update_window();

private:
    std::vector<AnimInterface*> *anim_interfaces;
    std::vector<int> sort_list;
    std::vector<bool> has_ended;
    bool ongoing;

    QLabel* number_of_items_label;
    QLCDNumber* number_of_items;
    QSlider* number_of_items_slider;

    QLabel* speed_label;
    QLCDNumber* speed;
    QSlider* speed_slider;

signals:

public slots:
    void sort();
    void step_by_step();
    void order_shuffle();
    void check();
    void stop();
};

#endif // ANIMWIN
