#include "shaker.h"
#include <iostream>

Shaker::Shaker(std::vector<int> items) : Sort(items)
{
    compute_sort(items);
}

void Shaker::compute_sort(std::vector<int> items)
{
    bool has_changed = true;
    bool l = true;
    while (has_changed)
    {
        has_changed = false;
        if (l)
        {
            for (int i = 0; i < items.size()-1; i++)
            {
                if (items[i] > items[i+1])
                {
                    has_changed = true;
                    swap(items, i, i+1);
                    add_instruction_swap(i+1, i);
                    add_instruction_color(i, Qt::red);
                }
                else
                {
                    add_instruction_color(i, Qt::green);
                }
            }
            l = false;
        }
        else
        {
            for (int j = items.size()-1; j > 0; j--)
            {
                if (items[j-1] > items[j])
                {
                    has_changed = true;
                    swap(items, j-1, j);
                    add_instruction_swap(j, j-1);
                    add_instruction_color(j, Qt::red);
                }
                else
                {
                    add_instruction_color(j, Qt::green);
                }
            }
            l = true;
        }
        add_instruction_all_color(Qt::gray);
    }
    add_instruction_all_color(Qt::green);
}
