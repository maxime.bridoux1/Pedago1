#ifndef SORT_H
#define SORT_H

#include "instruction.h"
#include <vector>
#include <QColor>

class Sort
{
public:
    Sort();
    Sort(std::vector<int> items);
    virtual void compute_sort(std::vector<int> items);
    Instruction get_next_instruction();
    void swap(std::vector<int> &items, int a, int b);
    bool is_done();

protected:
    int k;
    std::vector<Instruction> instructions;
    void add_instruction_swap(int a, int b);
    void add_instruction_color(int a, QColor c);
    void add_instruction_all_color(QColor c);
    void add_instruction_inter_color(int a, int b, QColor c);

};

#endif // SORT_H
