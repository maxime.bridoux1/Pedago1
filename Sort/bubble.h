#ifndef BUBBLE_H
#define BUBBLE_H

#include "sort.h"

class Bubble : public Sort
{
public:
	//Constructeur de base
	Bubble(std::vector<int> items);

	//Fonction de tri de base
        void compute_sort(std::vector<int> items);
};

#endif //BUBBLE_H

