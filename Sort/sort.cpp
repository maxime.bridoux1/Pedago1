#include "sort.h"

Sort::Sort() : k(0)
{

}

Sort::Sort(std::vector<int> items) : k(0)
{

}

void Sort::swap(std::vector<int> &items, int a, int b)
{
    int l = items[a];
    items[a] = items[b];
    items[b] = l;
}

void Sort::add_instruction_swap(int a, int b)
{
    instructions.push_back(Instruction(a, b, Qt::gray, 0));
}

void Sort::add_instruction_color(int a, QColor c)
{
    instructions.push_back(Instruction(a, -1, c, 1));
}

void Sort::add_instruction_all_color(QColor c)
{
    instructions.push_back(Instruction(-1, -1, c, 2));
}

void Sort::add_instruction_inter_color(int a, int b, QColor c)
{
    instructions.push_back(Instruction(a, b, c, 3));
}

Instruction Sort::get_next_instruction()
{
    return instructions[k++];
}

bool Sort::is_done()
{
    return (k == instructions.size());
}

void Sort::compute_sort(std::vector<int> items)
{

}
