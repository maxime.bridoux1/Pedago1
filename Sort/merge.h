#ifndef MERGE_H
#define MERGE_H

#include "sort.h"

class Merge : public Sort
{
public:
    //Constructeur de base
    Merge(std::vector<int> items);

    //Fonction de tri de base
    void compute_sort(std::vector<int> items);
    void cut(std::vector<int> &items, int i, int j);
    void fusion(std::vector<int> &items, int i,int j,int k,int l, int m);
};

#endif // MERGE_H
