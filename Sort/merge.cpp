#include "merge.h"
#include <iostream>

Merge::Merge(std::vector<int> items) : Sort(items)
{
    compute_sort(items);
}

void Merge::compute_sort(std::vector<int> items)
{
    cut(items, 0, items.size()-1);
}

void Merge::cut(std::vector<int> &items, int i, int j)
{
    if (i<j)
    {
       int m = std::abs((j+i)/2);
       add_instruction_color(m+1, Qt::blue);
       //std::cout << "Découpe 1 de " << i << " à " << m << std::endl;
       cut(items,i,m);
       add_instruction_inter_color(i,m,Qt::gray);
       //std::cout << "Découpe 2 de " << m+1 << " à " << j << std::endl;
       cut(items,m+1,j);
       add_instruction_inter_color(i,j,Qt::black);
       fusion(items,i,m,m+1,j,j);
    }
}

//fusionner de i à j et de k à l dans m
void Merge::fusion(std::vector<int> &items, int i, int j, int k, int l, int m)
{
    //std::cout << "Fusion de " << i << " à " << j << " et de " << k << " à " << l << " allant à " << m << std::endl;

    if((i<=j) && (k<=l))
    {
        if (items[j] > items [l])
        {
            swap(items, m, j);
            add_instruction_color(m,Qt::green);
            add_instruction_swap(j, m);
            for (unsigned int x = j; x < m - 1; x++)
            {
                swap(items, x, x+1);
                add_instruction_swap(x,x+1);
            }
            fusion(items, i, j-1, k-1, l-1, m-1);
        }
        else
        {
            add_instruction_color(m,Qt::green);
            fusion(items,i,j,k,l-1,m-1);
        }
    }
    else
    {
        add_instruction_inter_color(i,m,Qt::green);
    }
}
