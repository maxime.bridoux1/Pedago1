#include "quick.h"
#include <iostream>

Quick::Quick(std::vector<int> items) : Sort(items)
{
    compute_sort(items);    
}

void Quick::compute_sort(std::vector<int> items)
{
    quicksort(items, 0, items.size()-1);
    add_instruction_all_color(Qt::green);
}

void Quick::quicksort(std::vector<int> items, int left, int right)
{
    add_instruction_all_color(Qt::gray);
    add_instruction_inter_color(left,right,Qt::black);
    int i = left;
    int j = right;
    int m = (left+right)/2;
    int pivot = items[m];
    add_instruction_color(m, Qt::blue);

    while (i<=j)
    {
        add_instruction_color(i, Qt::red);
        while (items[i] < pivot)
        {
            add_instruction_color(i,Qt::green);
            i++;
            add_instruction_color(i, Qt::red);
        }
        add_instruction_color(j, Qt::red);
        while (items[j] > pivot)
        {
            add_instruction_color(j,Qt::green);
            j--;
            add_instruction_color(j, Qt::red);

        }
        if (i <= j)
        {
            swap(items,i,j);
            add_instruction_swap(i,j);
            add_instruction_color(i,Qt::green);
            add_instruction_color(j,Qt::green);
            i++;
            j--;
        }
    }
    if (left < j)
        quicksort(items, left, j);
    if (i < right)
        quicksort(items, i, right);
}
